extends Node

var player_node = preload("res://Player.tscn")
var doggo_node = preload("res://Doggo.tscn")
var girl_node = preload("res://Girl.tscn")
var baggage_node = preload("res://Baggage.tscn")

#player stats
var speed = 200.0
var talk_speed = 230.0
var talk_rate = 1.0
var hp = 1
var level = 0

#words
var complements = []
var cheers = []
var counsels = []
var contests = []
var insults = []

func _ready():
	randomize()
	
	insults.append("You're ugly!")
	insults.append("Nobody likes you!")
	insults.append("You're an asshole!")
	insults.append("I never want to see you again!")
	insults.append("I hate you!")
	insults.append("You're a loser!")
	insults.append("You have no friends!")
	
	complements.append("Great game!")
	cheers.append("My body is ready!")
	contests.append("Chill out, it's just a game...")
	counsels.append("It's okay if you aren't 100% finished")

func reset_stats():
	speed = 200.0
	talk_speed = 230.0
	talk_rate = 1.0
	hp = 1
	level = 0

func load_level():
	clear_words()
	get_tree().change_scene("res://Level.tscn")
	hp += 1
	level += 1

func clear_words():
	complements.clear()
	cheers.clear()
	counsels.clear()
	contests.clear()

func load_doggos_speak():
	complements.append("You're a good doggo!")
	complements.append("Who's a good doggo?")
	complements.append("Such a good doggo!")
	complements.append("Such a cutie pie!")
	
	cheers.append("Good job!")
	cheers.append("Well done!")
	
	contests.append("Easy there doggo")
	contests.append("Woah there doggo")
	
	counsels.append("No you are in fact the good boy")
	counsels.append("It's okay doggo, I forgive you")
	counsels.append("Everything is going to be alright")
	counsels.append("No one's gonna hurt you")

func load_girl_speak():
	complements.append("You're so pretty!")
	complements.append("You're cute!")
	complements.append("Thicc!")
	complements.append("You're so smart!")
	complements.append("Looking good!")
	complements.append("Nice shoes!")
	complements.append("You're glowing!")
	complements.append("You're so funny!")
	complements.append("You're a strong, independent woman who don't need no man!")
	
	cheers.append("Right on!")
	cheers.append("That a girl!")
	cheers.append("You tell'em girl!")
	
	contests.append("Chill out")
	contests.append("Take a chill pill")
	contests.append("Chillax")
	
	counsels.append("F the haters")
	counsels.append("You'll be alright")
	counsels.append("Stay strong")
	counsels.append("Everything will be alright")
	counsels.append("Would you like a hug?")
	counsels.append("Chin up")
	counsels.append("Turn that smile upside down")
