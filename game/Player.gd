extends KinematicBody2D

var words_node = preload("res://Words.tscn")

onready var sprite = get_node("Sprite")
onready var anim = get_node("AnimationPlayer")
onready var menu_anim = get_node("Menu/Overlay/AnimationPlayer")
onready var menu = get_node("Menu")
onready var damage_anim = get_node("DamageAnimationPlayer")

#talk types
const Complement = "Complement"
const Cheer = "Cheer"
const Contest = "Contest"
const Counsel = "Counsel"

#animation vars
var anim_speed_scale = 2.0

#directions
var up = deg2rad(0)
var down = deg2rad(180)
var left = deg2rad(90)
var right = deg2rad(-90)
var up_left = deg2rad(45)
var up_right = deg2rad(-45)
var down_left = deg2rad(135)
var down_right = deg2rad(-135)

#movement vars
var base_speed = 200.0
var speed = base_speed
var vel_up = Vector2(0.0, -1.0)
var vel_down = vel_up.rotated(down)
var vel_left = vel_up.rotated(left)
var vel_right = vel_up.rotated(right)
var vel_up_left = vel_up.rotated(up_left)
var vel_up_right = vel_up.rotated(up_right)
var vel_down_left = vel_up.rotated(down_left)
var vel_down_right = vel_up.rotated(down_right)

#words
var talk_speed = 230.0
var talk_rate = 1.0
var talk_cooldown = 0.0
var talk_type = Contest

func _ready():
	set_fixed_process(true)

func _fixed_process(delta):
	process_movement(delta)
	if(not menu.is_visible()):
		process_talking(delta)
	process_menu()

func process_menu():
	if(Input.is_action_pressed("Ammo_Select")):
		menu.show()
		
		if(Input.is_action_pressed("Shoot_Up")):
			menu_anim.play(Complement)
			talk_type = Complement
		elif(Input.is_action_pressed("Shoot_Down")):
			menu_anim.play(Counsel)
			talk_type = Counsel
		elif(Input.is_action_pressed("Shoot_Left")):
			menu_anim.play(Cheer)
			talk_type = Cheer
		elif(Input.is_action_pressed("Shoot_Right")):
			menu_anim.play(Contest)
			talk_type = Contest
		
	else:
		menu.hide()

func process_talking(delta):
	if(0.0 < talk_cooldown):
		talk_cooldown -= delta
	else:
		#shoot up_left
		if(Input.is_action_pressed("Shoot_Up") and not Input.is_action_pressed("Shoot_Down") and Input.is_action_pressed("Shoot_Left") and not Input.is_action_pressed("Shoot_Right")):
			talk(up_left)
			
		#shoot up_right
		elif(Input.is_action_pressed("Shoot_Up") and not Input.is_action_pressed("Shoot_Down") and not Input.is_action_pressed("Shoot_Left") and Input.is_action_pressed("Shoot_Right")):
			talk(up_right)
			
		#shoot down_left
		elif(not Input.is_action_pressed("Shoot_Up") and Input.is_action_pressed("Shoot_Down") and Input.is_action_pressed("Shoot_Left") and not Input.is_action_pressed("Shoot_Right")):
			talk(down_left)
			
		#shoot down_right
		elif(not Input.is_action_pressed("Shoot_Up") and Input.is_action_pressed("Shoot_Down") and not Input.is_action_pressed("Shoot_Left") and Input.is_action_pressed("Shoot_Right")):
			talk(down_right)
		
		#shoot up
		elif(Input.is_action_pressed("Shoot_Up") and not Input.is_action_pressed("Shoot_Down")):
			talk(up)
			
		#shoot down
		elif(not Input.is_action_pressed("Shoot_Up") and Input.is_action_pressed("Shoot_Down")):
			talk(down)
			
		#shoot left
		elif(Input.is_action_pressed("Shoot_Left") and not Input.is_action_pressed("Shoot_Right")):
			talk(left)
			
		#shoot right
		elif(not Input.is_action_pressed("Shoot_Left") and Input.is_action_pressed("Shoot_Right")):
			talk(right)
	
func process_movement(delta):
	var is_walking = true
	var vel
	
	if(Input.is_action_pressed("Move_Up") and not Input.is_action_pressed("Move_Down") and Input.is_action_pressed("Move_Left") and not Input.is_action_pressed("Move_Right")):
		vel = vel_up_left
		sprite.set_flip_h(true)
	elif(Input.is_action_pressed("Move_Up") and not Input.is_action_pressed("Move_Down") and not Input.is_action_pressed("Move_Left") and Input.is_action_pressed("Move_Right")):
		vel = vel_up_right
		sprite.set_flip_h(false)
	elif(not Input.is_action_pressed("Move_Up") and Input.is_action_pressed("Move_Down") and Input.is_action_pressed("Move_Left") and not Input.is_action_pressed("Move_Right")):
		vel = vel_down_left
		sprite.set_flip_h(true)
	elif(not Input.is_action_pressed("Move_Up") and Input.is_action_pressed("Move_Down") and not Input.is_action_pressed("Move_Left") and Input.is_action_pressed("Move_Right")):
		vel = vel_down_right
		sprite.set_flip_h(false)
	elif(Input.is_action_pressed("Move_Up") and not Input.is_action_pressed("Move_Down")):
		vel = vel_up
	elif(not Input.is_action_pressed("Move_Up") and Input.is_action_pressed("Move_Down")):
		vel = vel_down
	elif(Input.is_action_pressed("Move_Left") and not Input.is_action_pressed("Move_Right")):
		vel = vel_left
		sprite.set_flip_h(true)
	elif(not Input.is_action_pressed("Move_Left") and Input.is_action_pressed("Move_Right")):
		vel = vel_right
		sprite.set_flip_h(false)
	else:
		vel = Vector2()
		is_walking = false
	
	if(is_walking and anim.get_current_animation() != "walk"):
		var anim_speed = speed/base_speed * anim_speed_scale
		anim.play("walk", 0, anim_speed)
	elif(not is_walking and anim.get_current_animation() != "base"):
		anim.play("base")
	
	move_and_slide(vel*speed)
	
func talk(angle):
	#reset cooldown
	talk_cooldown = talk_rate
	
	#instance a words node
	var words = words_node.instance()
	words.vel = (vel_up * talk_speed).rotated(angle)
	words.talk_type = talk_type
	words.set_global_pos(get_pos())
	
	#set text
	if(talk_type == Complement):
		words.set_text(Global.complements[randi() % Global.complements.size()])
	elif(talk_type == Cheer):
		words.set_text(Global.cheers[randi() % Global.cheers.size()])
	elif(talk_type == Counsel):
		words.set_text(Global.counsels[randi() % Global.counsels.size()])
	elif(talk_type == Contest):
		words.set_text(Global.contests[randi() % Global.contests.size()])
	else:
		words.set_text("Error: undefined talk setting")
	
	get_parent().add_child(words)

func die():
	set_fixed_process(false)
	anim.play("death")

func _on_RestartButton_pressed():
	Global.reset_stats()
	Global.load_level()

func take_criticism():
	if(anim.get_current_animation() != "death"):
		damage_anim.play("hit")
		get_parent().remove_health()

func take_complement():
	get_parent().add_health()

func listen(talk_type):
	take_criticism()