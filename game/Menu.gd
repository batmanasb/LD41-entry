extends Control

func _ready():
	pass

func _on_Play_pressed():
	Global.load_level()


func _on_Help_pressed():
	get_node("Label").show()


func _on_Quit_pressed():
	get_tree().quit()
