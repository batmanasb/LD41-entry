extends KinematicBody2D

onready var anim1 = get_node("AnimationPlayer1")
onready var anim2 = get_node("AnimationPlayer2")
onready var eyes = get_node("Skins/Eyes")

#talk types
const Complement = "Complement"
const Cheer = "Cheer"
const Contest = "Contest"
const Counsel = "Counsel"

#states
const NORMAL = 1
const SAD = 2
const HAPPY = 3
const ANGRY = 4

#stats
var speed = 200.0
var angry_speed_scale = 1.5
var state = ANGRY
onready var target_location = get_pos()

#animation vars
var anim_speed_scale = 2.0

#running into corners
var corners = [Vector2(100, 100), Vector2(924, 100), Vector2(100, 500), Vector2(924, 500)]

#running in circles
var circle_origin = Vector2()
var circle_radius = Vector2(0,-20)
var circle_segment_lenght = deg2rad(10)
var target_angle = 0

#walking around
var walk_radius = Vector2(0, -300)

func _ready():
	set_fixed_process(true)
	anim2.play("walk", 0, anim_speed_scale)
	
	#pick a random skin
	var selection = rand_range(0,3)
	get_node("Skins").get_child(selection).show()

func listen(talk_type):
	if(anim1.get_current_animation() != "fade_away"):
		anim1.play("hit")
		
		if(talk_type == Complement):
			if(state == NORMAL):
				state = HAPPY
				circle_origin = Vector2(randf() * 624 + 200, randf() * 200 + 200)
				target_location = circle_origin
		elif(talk_type == Cheer):
			if(state == HAPPY):
				anim1.play("fade_away")
		elif(talk_type == Counsel):
			if(state == SAD):
				state = NORMAL
				anim2.play("walk", 0, anim_speed_scale)
				eyes.hide()
		elif(talk_type == Contest):
			if(state == ANGRY):
				state = NORMAL
				eyes.hide()


func _on_Area2D_body_enter( body ):
	if(state == NORMAL or state == ANGRY):
		if(body.has_method("take_criticism")):
			body.take_criticism()
			state = SAD
			find_nearest_corner()
			eyes.hide()

func find_nearest_corner():
	target_location = corners[0]
	var shortest_distance = corners[0].distance_to(get_pos())
	for i in range(1,3):
		var distance = corners[i].distance_to(get_pos())
		if(distance < shortest_distance):
			shortest_distance = distance
			target_location = corners[i]

func vanish():
	queue_free()

func _fixed_process(delta):
	if(state == NORMAL):
		if(get_pos().distance_to(target_location) < 5.0):
			target_location = get_pos() + walk_radius.rotated(deg2rad(randi()%360))
			while(target_location.x > 924):
				target_location.x -= 30.0
			while(target_location.y > 500):
				target_location.y -= 30.0
			while(target_location.x < 100):
				target_location.x += 30.0
			while(target_location.y < 100):
				target_location.y += 30.0
		var dir = target_location - get_pos()
		move_and_slide(dir.normalized() * speed)
	elif(state == SAD):
		if(get_pos().distance_to(target_location) > 20.0):
			var dir = target_location - get_pos()
			move_and_slide(dir.normalized() * speed)
		else:
			if(anim2.get_current_animation() != "base"):
				anim2.play("base")
	elif(state == HAPPY):
		if(get_pos().distance_to(target_location) < 5.0):
			target_angle += circle_segment_lenght
			if(target_angle > 360):
				target_angle = 0
			target_location = circle_origin + circle_radius.rotated(target_angle)
		var dir = target_location - get_pos()
		move_and_slide(dir.normalized() * speed)
	elif(state == ANGRY):
		if(get_pos().distance_to(target_location) < 5.0):
			target_location = get_pos() + walk_radius.rotated(deg2rad(randi()%360))
			while(target_location.x > 924):
				target_location.x -= 30.0
			while(target_location.y > 500):
				target_location.y -= 30.0
			while(target_location.x < 100):
				target_location.x += 30.0
			while(target_location.y < 100):
				target_location.y += 30.0
		var dir = target_location - get_pos()
		move_and_slide(dir.normalized() * speed * angry_speed_scale)
	
	#face direction of movement
	if(target_location.x < get_pos().x):
		set_scale(Vector2(1, 1))
	else:
		set_scale(Vector2(-1, 1))