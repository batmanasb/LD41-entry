extends KinematicBody2D

var insults_node = preload("res://Insults.tscn")

onready var walk_anim = get_node("WalkAnim")
onready var hit_anim = get_node("HitAnim")
onready var sprite = get_node("Sprite")

#talk types
const Complement = "Complement"
const Cheer = "Cheer"
const Contest = "Contest"
const Counsel = "Counsel"

#states
const NORMAL = 1
const SAD = 2
const HAPPY = 3
const ANGRY = 4

var state = ANGRY
var vel_up = Vector2(0.0, -1.0)
var corners = [Vector2(100, 100), Vector2(924, 100), Vector2(100, 500), Vector2(924, 500)]
onready var target_location = corners[randi()%4]
var aim = 0
var is_aiming = false
var tantrum_duration = 5.0
var cooldown = tantrum_duration
var talk_cooldown = 0.0
var talk_rate = 0.1
var walking_talk_rate = 0.5
var walking_talk_cooldown = 1.0
var talk_speed = 230.0
var speed = 300.0
var emotional_baggage = 3
var bag_drops_remaining = 3
var walk_radius = Vector2(0, -300)

func _ready():
	set_fixed_process(true)
	walk_anim.play("walk", 0, 3)

func _fixed_process(delta):
	if(state == NORMAL):
		if(get_pos().distance_to(target_location) < 5.0):
			target_location = get_pos() + walk_radius.rotated(deg2rad(randi()%360))
			while(target_location.x > 924):
				target_location.x -= 30.0
			while(target_location.y > 500):
				target_location.y -= 30.0
			while(target_location.x < 100):
				target_location.x += 30.0
			while(target_location.y < 100):
				target_location.y += 30.0
		var dir = target_location - get_pos()
		move_and_slide(dir.normalized() * speed)
		
		if(walking_talk_cooldown < 0):
			walking_talk_cooldown = walking_talk_rate
			shout(rand_range(deg2rad(0), deg2rad(360)))
		else:
			walking_talk_cooldown -= delta
	elif(state == SAD):
		pass
	elif(state == HAPPY):
		pass
	elif(state == ANGRY):
		if(emotional_baggage <= 0):
			state = NORMAL
		else:
			if(cooldown < 0):
				is_aiming = false
				target_location = corners[randi()%4]
				cooldown = tantrum_duration
			if(not is_aiming and get_pos().distance_to(target_location) < 20):
				aim = rad2deg(get_pos().angle_to_point(Vector2(512,300)))
				is_aiming = true
				if(target_location.x < get_pos().x):
					set_scale(Vector2(-1, 1))
				else:
					set_scale(Vector2(1, 1))
			elif(is_aiming):
				cooldown -= delta
				if(cooldown < 0):
					drop_baggage()
				if(talk_cooldown < 0):
					talk_cooldown = talk_rate
					shout(rand_range(deg2rad(int(aim) - 65), deg2rad(int(aim) + 65)))
				else:
					talk_cooldown -= delta
			else:
				var dir = target_location - get_pos()
				move_and_slide(dir.normalized() * speed)
	
	if(not is_aiming):
		if(target_location.x < get_pos().x):
			set_scale(Vector2(1, 1))
		else:
			set_scale(Vector2(-1, 1))

func listen(talk_type):
	if(hit_anim.get_current_animation() != "fade_away"):
		hit_anim.play("hit")
		
		if(talk_type == Complement):
			speed *= 0.8
		elif(talk_type == Cheer):
			walking_talk_rate *= 0.5
		elif(talk_type == Counsel):
			if(state == NORMAL):
				speed -= 100
				if(speed < 0):
					speed = 0
					hit_anim.play("fade_away")
		elif(talk_type == Contest):
			if(state == ANGRY):
				talk_rate *= 1.5
			if(state == NORMAL):
				walking_talk_rate *= 1.5

func shout(angle):
	#reset cooldown
	talk_cooldown = talk_rate
	
	#instance a insults node
	var insults = insults_node.instance()
	insults.vel = (vel_up * talk_speed).rotated(angle)
	insults.set_global_pos(get_pos())
	insults.set_text(Global.insults[randi() % Global.insults.size()])
	
	get_parent().add_child(insults)

func vanish():
	queue_free()

func relax_a_little():
	emotional_baggage -= 1

func drop_baggage():
	if(bag_drops_remaining > 0):
		bag_drops_remaining -= 1
		var bag = Global.baggage_node.instance()
		bag.owner = self
		bag.set_pos(get_pos())
		get_parent().add_child(bag)