extends Area2D

onready var anim = get_node("AnimationPlayer")

var owner = null
var used = false

func _ready():
	#pick a random skin
	var selection = rand_range(0,3)
	get_node("Skins").get_child(selection).show()


func _on_Baggage_body_enter( body ):
	if(not used):
		used = true
		owner.relax_a_little()
		anim.play("fade_away")

func vanish():
	queue_free()