extends Area2D

var active = true
var vel = Vector2()
var talk_type = "Complement"

func _ready():
	set_fixed_process(true)

func _fixed_process(delta):
	set_pos(get_pos() + vel * delta)


func _on_CollisionShape2D_enter_tree():
	pass # replace with function body


func _on_Words_body_enter( body ):
	if(active):
		active = false
		
		#relay info to hit body
		if(body.has_method("listen")):
			body.listen(talk_type)
			
		#hide and delete self
		set_fixed_process(false)
		set_opacity(0)
		queue_free()

func set_text(text):
	get_node("Control/Label").set_text(text)