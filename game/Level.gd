extends Node2D

onready var health_bar = get_node("PlayerHP")
onready var mobs = get_node("Mobs")

var location_player = Vector2(512, 400)

func _ready():
	#initialize player's health bar
	set_health(Global.hp)
	
	#spawn player
	var player = Global.player_node.instance()
	player.set_global_pos(location_player)
	player.speed = Global.speed
	player.talk_speed = Global.talk_speed
	player.talk_rate = Global.talk_rate
	add_child(player)
	
	if(Global.level % 3 == 0):
		Global.load_girl_speak()
		spawn_girls(int(Global.level/3))
	else:
		Global.load_doggos_speak()
		spawn_doggos(Global.level - int(Global.level/3))
	
	set_process(true)

func _process(delta):
	if(mobs.get_child_count() == 0):
		Global.load_level()

func add_health():
	set_health(health_bar.get_child_count() + 1)

func remove_health():
	Global.hp -= 1
	if(health_bar.get_child_count() > 1):
		set_health(health_bar.get_child_count() - 1)
	else:
		#kill player and clear level and HUD
		get_node("Player").die()
		get_node("Mobs").hide()
		health_bar.hide()

func set_health(hp):
	while(health_bar.get_child_count() > 1):
		health_bar.get_child(health_bar.get_child_count()-1).free()
	var heart = health_bar.get_node("Sprite")
	var spacing = 10
	for i in range(1, hp):
		var new_heart = heart.duplicate()
		new_heart.set_pos(heart.get_pos() + Vector2(spacing * i, 0))
		heart.get_parent().add_child(new_heart)

func spawn_doggos(amount):
	for i in range(amount):
		var doggo = Global.doggo_node.instance()
		var location = location_player
		while(location.distance_to(location_player) < 200.0):
			location = Vector2(randf() * 824 + 100, randf() * 400 + 100)
		doggo.set_global_pos(location)
		mobs.add_child(doggo)

func spawn_girls(amount):
	for i in range(amount):
		var girl = Global.girl_node.instance()
		var location = location_player
		while(location.distance_to(location_player) < 200.0):
			location = Vector2(randf() * 824 + 100, randf() * 400 + 100)
		girl.set_global_pos(location)
		mobs.add_child(girl)